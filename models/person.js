var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
	name: {type: String},
	phone_number:{ type: String},
	city: {type: String}
});

var Person = mongoose.model('Person', schema);

// make this available to our users in our Node applications
module.exports = {
  Person:Person
}
